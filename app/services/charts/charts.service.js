angular.module('Charts')
	.factory('Charts', ['$http', function($http){
		var service  = {};
		//chartsArray to store all charts
		var chartsArray = [];

		//constants
		var GRAPHSMALL = {
			HEIGHT:200,
			WIDTH:300
		};
		var GRAPHMEDIUM = {
			HEIGHT:300,
			WIDTH: 500
		};
		var GRAPHBIG = {
			HEIGHT:500,
			WIDTH:800
		};


	//getdata from data source
	//append string from UI
	service.getData = function(source){
		return $http.get('data/' + source + '.json');
	};



		//getChartsArray function
		service.getChartsArray = function(){
			return chartsArray;
		}

	//createGraph function
	service.createGraph = function(labels,series,data,onClick,datasetOverride,options,type,height,width,title,color){
		var graph = {};
		graph.labels = labels;
		graph.series = series;
		graph.data = data;
		graph.onClick = onClick;
		graph.datasetOverride = datasetOverride;
		graph.options = options;
		graph.type = type;
		graph.height = height;	
		graph.width = width;	
		graph.title = title;

		if(type !== 'pie'){
				graph.colors = color;
			}
		chartsArray.push(graph); 
	}


		service.determineSize = function(size){
			var chartSize = '';
			if(size === "small"){
				chartSize = GRAPHSMALL;
			}else if(size === "medium"){
				chartSize = GRAPHMEDIUM;
			}else if(size === "big"){
				chartSize = GRAPHBIG;
			};
			return chartSize;
		};

	//removeGraph function
	service.remove = function(index){
		chartsArray.splice(index,1);
	}

	service.extractDraft = function(response,xaxis,yaxis){
		var draft = {};
		var dataArray = [];
		var innerDataArray = [];
		var labelsArray = [];
		var axes = [];
		var seriesArray = [];
		var labelsCounter = 0;
		var oneLoop = 0;
		angular.forEach(response, function(value, key){
			innerDataArray = [];
			seriesArray.push(key);
			// console.log('series: ' + key);
			angular.forEach(value, function(value_in, key_in){					
				angular.forEach(value_in, function(value_in_in, key_in_in){
					// console.log(key_in_in + ": " + value_in_in);
					//key becomes x-axis or y-axis
					if (key_in_in === yaxis){
						innerDataArray.push(value_in_in);
					}
					if(key_in_in === xaxis && !labelsCounter){
						labelsArray.push(value_in_in);
						// console.log(value_in_in);
					}
					if(!oneLoop){
							axes.push(key_in_in);						
					}
				});
				oneLoop++;
			});
			labelsCounter++;
			dataArray.push(innerDataArray);
		});
		// console.log(dataArray);
		// console.log(labelsArray);
		// console.log(axes);
		draft.series = seriesArray;
		draft.data = dataArray;
		draft.labels = labelsArray;
		draft.axes = axes;

		return draft;
	}

	return service;
}])

