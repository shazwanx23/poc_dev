angular.module('pieChart')
.component('pieChart', {
	templateUrl: 'pie-chart/pie-chart.template.html',
	controller: ['$scope', function($scope){
		$scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
		$scope.data = [300, 500, 100];
	}]
});