/**
 * Created by azira.aziz on 10/3/2016.
 */
angular.module('lineChart')
    .component('lineChart',{
        templateUrl: 'line-chart/line-chart.template.html',
        controller: ['$scope', 'Charts',  function ChartController($scope,Charts){
            //  details to make line graph
            $scope.labels = [];
            $scope.series =  ['Series A', 'Series B'];
            $scope.data = [];
            Charts.getData().success(function(response){
                angular.forEach(response, function(key, value){
                    var dataArray = [];
                    angular.forEach(key, function(key_in,value_in){
                        dataArray.push(key_in); //data
                        if(!value){
                            $scope.labels.push("" + value_in); //month
                        }

                    });
                    $scope.data.push(dataArray);

                    console.log('value: ' + value); //series
                });
                console.log($scope.labels);
                console.log($scope.data);

            });


            $scope.onClick = function (points, evt) {
                console.log(points, evt);
            };
            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
            $scope.options = {
                scales: {
                    yAxes: [
                        {
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left'
                        },
                        {
                            id: 'y-axis-2',
                            type: 'linear',
                            display: true,
                            position: 'right'
                        }
                    ]
                }
            };

        }]
    });