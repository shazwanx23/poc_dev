angular.module('myApp.dashboard')
	.component('dashboard',{
		templateUrl: 'dashboard/dashboard.template.html',
		controller: ['$scope', 'Charts', 'Products',  function ChartController($scope,Charts,Products){

			// a single graph object which will store all the information required to
			//display a single graph

			//constants
			var GRAPHSMALL = {
				HEIGHT:200,
				WIDTH:300
			};
			var GRAPHMEDIUM = {
				HEIGHT:300,
				WIDTH: 500
			};
			var GRAPHBIG = {
				HEIGHT:500,
				WIDTH:800
			};

			$scope.refreshGraphs = function(){
				$scope.graphs = Charts.getChartsArray();
			};
			$scope.remove = function(index){
				Charts.remove(index);
				$scope.refreshGraphs();
			}
			$scope.graphs = [];
			$scope.selectedXaxis = '';
			$scope.selectedYaxis = '';
			$scope.addChart = function(chart,xaxis,yaxis){
				var draft = Charts.extractDraft($scope.response,xaxis,yaxis);
				var labelsArray = draft.labels;
				var seriesArray = draft.series;
				var dataArray = draft.data;
				var colours = [];
				var sizeOfChart = Charts.determineSize(chart.inputSize);

				for(var i=0; i<$scope.colors.length; i++){
						colours.push($scope.colors[i].color);

				}

				chart.color = colours;


				Charts.createGraph(
					labelsArray,
					seriesArray,
					dataArray,
					null,null,null,
					chart.inputWidgetType,
					sizeOfChart.HEIGHT,
					sizeOfChart.WIDTH,
					chart.inputTitle,
					chart.color
				);
				$scope.refreshGraphs();

			}
			//end form data

			//start draggable of chart
			$scope.onDropComplete = function (index, graph, evt) {
				var otherObj = $scope.graphs[index];
				var otherIndex = $scope.graphs.indexOf(graph);
				$scope.graphs[index] = graph;
				$scope.graphs[otherIndex] = otherObj;
			}
			//end draggable of chart

			//parse json file into series and data
			$scope.axes = [];
			var oneLoop = 0;
			$scope.response = '';
			$scope.getDataSource = function(source){
				Charts.getData(source).success(function(response){
					$scope.response = response;
					var draft = Charts.extractDraft(response,0,0);
					$scope.axes = draft.axes;
					$scope.series = $scope.getSeries(draft.series);
					$scope.colors = $scope.getColors(draft.series);
				});
			}
			//color and series
			// $scope.series = [];
			$scope.colors = [];

			$scope.getSeries = function(seriesArray){
				var seriesObject = [];
				for(var i=0; i<seriesArray.length; i++){
					seriesObject.push({series: seriesArray[i]});
				}
				return seriesObject;
			}
			$scope.getColors = function(seriesArray){
				var colors = [];
				for(var i=0; i<seriesArray.length; i++){
					colors.push({colors: $scope.colors[i]});
				}
				return colors;
			}

			// start add row form datasource
			$scope.items = [{datasource:'line.json'}];

			$scope.addNewRow = function(){
				var rowNo = $scope.items.length+1;
				$scope.items.push({datassource:'line.json'});
			}

			$scope.removeChoice = function() {
				var lastItem = $scope.items.length-1;
				$scope.items.splice(lastItem);
			};
			// end add row form datasource
		}]
	});