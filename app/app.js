'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'Charts',
  'myApp.dashboard'
  ,'kendo.directives',
  'Products'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/dashboard'});
  $routeProvider.
    when('/dashboard', {
       template: '<dashboard></dashboard>' + 
                  '<modal></modal>'

    })
}]);
